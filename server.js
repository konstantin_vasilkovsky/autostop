var express = require('express');
var app = express();
var path = require('path');
var fs = require('fs');
var http = require('http');
var https = require('https');
var privateKey  = fs.readFileSync('sslcert/server.key', 'utf8');
var certificate = fs.readFileSync('sslcert/server.crt', 'utf8');

var credentials = {key: privateKey, cert: certificate};

var httpServer = http.createServer(app);
var httpsServer = https.createServer(credentials, app);

app.use(express.static(path.join(__dirname, 'public')));
app.set('views',path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/',  function(req, res){
	res.render('index');
});

httpServer.listen(3000);
httpsServer.listen(3001);