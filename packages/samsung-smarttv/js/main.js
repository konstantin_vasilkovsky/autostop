'use strict'
var player;
var iframe = $('#player');
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '390',
        width: '640',
        videoId: '0Bmhjf0rKe8',
        playerVars: {
            'autoplay': 0,
            'controls': 1
        },
        events: {
            onReady: onPlayerReady,
        }
    });
}
var playerReady = false;

function onPlayerReady(event) {
    playerReady = true;
}

Smaf.ready(function() {

	var API_KEY = 'AIzaSyCVbVUBueHLbxBUxVPRrMCv6yUqyU4Q9Ec';
	var plids = [];
	var nextPageToken = '';
	var nav = {
		pl: {
			active: 0,
			fired: false
		},
		v: {
			row: 0,
			cv: 0,
			fs: false
		}
	};

	function init(){
		var playlist = '';
		$.get('https://www.googleapis.com/youtube/v3/playlists?part=snippet,contentDetails&channelId=UCsUoL8RwXnVT7wa3uLe3lpg&maxResults=16&key=' + API_KEY, function(data){
			data.items.forEach(function(it){
				playlist +=
					"<li class='border' data-url='"+it.id+"' data-name='"+it.snippet.title+"'>"
					+ "<img src='" + it.snippet.thumbnails.default.url + "'>"
					+ "<p>" + it.snippet.title+ "</p>"
					+"</li>";
			});
			$('.playlist').html(playlist);
			initControl();
			$('.playlist li')[0].focus();
			$($('.playlist li')[0]).addClass('active');
			$('.playlist').on('click enter', 'li', function(event){
				pickPlaylist(this);
			});
		}).fail(function() {alert( "error" );});
	}
	function scroll(nextPageToken, vURL, API_KEY){
		var loaded = false;
		$(window).scroll(function(){
			if(($(window).height() + $(window).scrollTop() + 200  > $('.videolist').offset().top + $('.videolist').outerHeight()) && nextPageToken && !loaded){
				loaded = true;
				$.get("https://www.googleapis.com/youtube/v3/playlistItems?pageToken="+nextPageToken+"&part=snippet&maxResults=16&playlistId="+vURL+"&&key=" + API_KEY, function(res){
					loaded = false;
					var videolists = "<div class='row'>";
					nextPageToken = res.nextPageToken || '';
					res.items.forEach(function(v, i){
						if(i%4 === 0 && i > 0){videolists += "</div><div class='row'>";}
						videolists +=
							"<div class='col-lg-3 li'><iframe width='100%'  src='//www.youtube.com/embed/"+v.id.videoId+"' frameborder='0' allowfullscreen='allowfullscreen' oallowfullscreen='oallowfullscreen' webkitallowfullscreen='webkitallowfullscreen'></iframe><p>"+v.snippet.title+"</p></div>";
					});
					$('.videolist').append(videolists);
				});
			}
		}); 
	};

	function pickPlaylist(that){
		$('.playlist li').removeClass('active');
		$('#pl-name').text(($(that).data('name')));
		$(that).addClass('active');
		var vURL = $(that).data('url');
		$.get('https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=16&playlistId='+vURL+'&key=' + API_KEY, function(data){
			var videolists = "<div class='row'>";
			nextPageToken = data.nextPageToken || '';
			data.items.forEach(function(v, i){
				if(i%4 === 0 && i > 0){videolists += "</div><div class='row'>";}
				videolists +=
					"<div class='col-lg-3 li' data-videoid='"+v.contentDetails.videoId+"'><iframe width='100%'  src='//www.youtube.com/embed/"+v.contentDetails.videoId+"' frameborder='0' allowFullScreen></iframe><p>"+v.snippet.title+"</p></div>";
			});
			$('.videolist').html(videolists);
			$('.videolist .li')[0].focus();
			$($('.videolist .li')[0]).addClass('active');
	       	scroll(nextPageToken, vURL, API_KEY);
		});
	};

	function pickVideo(){
		$('.videolist .li').removeClass('active');
		$($($('.videolist .row')[nav.v.row]).find('.li')[nav.v.cv]).addClass('active');
	};
	function initControl(){
		Smaf.on('action', function(evt) {
			console.log("EVENT", evt);
			if(evt.type == 'keyUp'){
				if(!nav.pl.fired){
					switch(evt.command){
						case "UP" : if(nav.pl.active != 0){
								var h = $('.playlist li').outerHeight() + 20;
								$(".playlist").css({top: $(".playlist").position().top += h});
								--nav.pl.active;
							};
						break;
						case "DOWN" : if(nav.pl.active+1 < $('.playlist li').length){
								var h = $('.playlist li').outerHeight() + 20;
								$(".playlist").css({top: $(".playlist").position().top -= h});
								++nav.pl.active;
							};
						break;
						case "ENTER": 
								nav.pl.fired = true;
								$("#leftCol").addClass('pos-fixed');
								$($('.playlist li')[nav.pl.active]).focus();
								pickPlaylist($($('.playlist li')[nav.pl.active]));
								break;
						case "RIGHT": 
								nav.pl.fired = true;
								$("#leftCol").addClass('pos-fixed');
								$($('.playlist li')[nav.pl.active]).focus();
								pickPlaylist($($('.playlist li')[nav.pl.active]));
								break;
					}
					$($('.playlist li')[nav.pl.active]).focus();
					$('.playlist li').removeClass('active');
					$($('.playlist li')[nav.pl.active]).addClass('active');	
				} else if(nav.pl.fired && !nav.v.fs){
					var rh = $('.videolist .row').outerHeight();
					switch(evt.command){
						case "UP" :
							if(nav.v.row != 0){
								--nav.v.row;
								$(".videolist").css({top: $(".videolist").position().top += rh});
								pickVideo();
							}
							break;
						case "DOWN" : 
							if(nav.v.row+1 < $('.videolist .row').length){
								++nav.v.row;
								$(".videolist").css({top: $(".videolist").position().top -= rh});
								pickVideo();
							}
							break;
						case "RIGHT" : 
							if(nav.v.cv+1 < $($('.videolist .row')[nav.v.row]).find('.li').length){
								++nav.v.cv;
								pickVideo();
							}
							break;
						case "LEFT" : 
							if(nav.v.cv != 0){
								--nav.v.cv;
								pickVideo();
							}
							break;
						case "ENTER":
							player.loadVideoById($('.row .li.active').data('videoid'), 0, "default");
							$('#player').removeClass('hidden');
							var requestFullScreen = player.a.requestFullscreen || player.a.msRequestFullscreen || player.a.mozRequestFullScreen || player.a.webkitRequestFullscreen;
							if (requestFullScreen) {
							    requestFullScreen.bind(player.a)();
							    nav.v.fs = true;
							}
							break;
						case "BACK": 
							nextPageToken = '';
							nav.pl.fired = false;
							$('.videolist .li').removeClass('active');
							nav.v.row = 0;
							nav.v.cv = 0;
							break;
					}
				} else {
					switch(evt.command){
						case "BACK": 
							console.log("PLAYER", player);
							$('#player').addClass('hidden');
							player.stopVideo();
							nav.v.fs = false;
							break;
					}
				}
			}
		});
	};
	$(document).ready(function(){
		init();
	});
});
Smaf.init('W60xBbHl0RB4ByZVqqelFcUOAgdbzCe1');