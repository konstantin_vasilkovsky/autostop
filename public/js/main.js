'use strict';
$.support.cors = true;
// var widgetAPI = new Common.API.Widget();
// var pluginAPI = new Common.API.Plugin();
// var tvKey = new Common.API.TVKeyValue();
var player;
var iframe = $('#player');
function onYouTubeIframeAPIReady() {
    player = new YT.Player('player', {
        height: '720',
        width: '1280',
        videoId: '',
        playsinline: 0,
        playerVars: {
            'autoplay': 0,
            'controls': 0
        },
        events: {
            onReady: onPlayerReady,
        }
    });
}
var playerReady = false;

function onPlayerReady(event) {
    playerReady = true;
}

Smaf.ready(function() {
	var API_KEY = 'AIzaSyCVbVUBueHLbxBUxVPRrMCv6yUqyU4Q9Ec';
	var plids = [];
	var nextPageToken = '';
	var nav = {
		pl: {
			active: 0,
			fired: false
		},
		v: {
			row: 0,
			cv: 0,
			fs: false
		}
	};

	function init(){
		var playlist = '';
		$.get('https://www.googleapis.com/youtube/v3/playlists?part=snippet,contentDetails&channelId=UCsUoL8RwXnVT7wa3uLe3lpg&maxResults=16&key=' + API_KEY, function(data){
			data.items.forEach(function(it, i){
				playlist +=
					"<li class='border' id='"+i+"'data-url='"+it.id+"' data-name='"+it.snippet.title+"'>"
					// + "<img src='" + it.snippet.thumbnails.default.url + "'>"
					+ "<p>" + it.snippet.title+ "</p>"
					+"</li>";
			});
			$('.playlist').html(playlist);
			initControl();
			$('.playlist li')[0].focus();
			$($('.playlist li')[0]).addClass('active');
			// $('.playlist').on('enter', 'li', function(event){
			// 	pickPlaylist($(this));
			// });
		}).fail(function() {alert( "error" );});
	}
	function scroll(nextPageToken, vURL, API_KEY){
		var loaded = false;
		$(window).scroll(function(){
			if(($(window).height() + $(window).scrollTop() + 200  > $('.videolist').offset().top + $('.videolist').outerHeight()) && nextPageToken && !loaded){
				loaded = true;
				$.get("https://www.googleapis.com/youtube/v3/playlistItems?pageToken="+nextPageToken+"&part=snippet&maxResults=16&playlistId="+vURL+"&&key=" + API_KEY, function(res){
					loaded = false;
					var videolists = "<div class='row'>";
					nextPageToken = res.nextPageToken || '';
					res.items.forEach(function(v, i){
						if(i%3 === 0 && i > 0){videolists += "</div><div class='row'>";}
						videolists +=
							"<div class='col-sm-4 li' data-videoid='"+v.id.videoId+"' id='"+v.id.videoId+"'><img width='100%'  src='"+v.snippet.thumbnails.high.url+"'><p>"+v.snippet.title+"</p></div>";
					});
					$('.videolist').append(videolists);
				});
			}
		}); 
	};

	function pickPlaylist(){
		var that = $('.playlist li#'+nav.pl.active);
		console.log(that.selector);
		$('.playlist li').removeClass('active');
		$('#pl-name').text(that.attr('data-name'));
		that.addClass('active');
		var vURL = that.attr('data-url');
		console.log("THAT URL1 " + vURL);
		$.ajax({
		    url: 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet,contentDetails&maxResults=16&playlistId='+vURL+'&key=' + API_KEY,
		    type: 'GET',
		    success: function(data){ 
				var videolists = "<div class='row'>";
				nextPageToken = data.nextPageToken || '';
				data.items.forEach(function(v, i){
					if(i%3 === 0 && i > 0){videolists += "</div><div class='row'>";}
					videolists +=
						"<div class='col-sm-4 li' data-videoid='"+v.contentDetails.videoId+"' id='"+v.contentDetails.videoId+"'><img width='100%'  src='"+v.snippet.thumbnails.high.url+"'><p>"+v.snippet.title+"</p></div>";
				});
				$('.videolist').html(videolists).show();
				console.log("$('.videolist') " + $('.videolist .li').length)
				$('.videolist .li')[0].focus();
				$($('.videolist .li')[0]).addClass('active');
		       	scroll(nextPageToken, vURL, API_KEY);
		    },
		    error: function(data, status, error) {
		    	console.log(data.responseText);
		        alert('woops!'); //or whatever
		    }
		});
	};

	function pickVideo(){
		$('.videolist .li').removeClass('active');
		$($($('.videolist .row')[nav.v.row]).find('.li')[nav.v.cv]).addClass('active');
	};
	function initControl(){
		Smaf.on('action', function(evt) {
			if(evt.type == 'keyUp'){
				if(!nav.pl.fired){
					switch(evt.command){
						case "UP" : if(nav.pl.active != 0){
								var h = $('.playlist li').outerHeight() + 20;
								$(".playlist").css({top: $(".playlist").position().top += h});
								--nav.pl.active;
							};
						break;
						case "DOWN" : if(nav.pl.active+1 < $('.playlist li').length){
								var h = $('.playlist li').outerHeight() + 20;
								$(".playlist").css({top: $(".playlist").position().top -= h});
								++nav.pl.active;
							};
						break;
						case "ENTER": 
								nav.pl.fired = true;
								$("#leftCol").addClass('pos-fixed');
								$($('.playlist li')[nav.pl.active]).focus();
								pickPlaylist($('.playlist #'+nav.pl.active));
								break;
						case "RIGHT": 
								nav.pl.fired = true;
								$("#leftCol").addClass('pos-fixed');
								$($('.playlist li')[nav.pl.active]).focus();
								pickPlaylist($('.playlist #'+nav.pl.active));
								break;
					}
					$($('.playlist li')[nav.pl.active]).focus();
					$('.playlist li').removeClass('active');
					$($('.playlist li')[nav.pl.active]).addClass('active');	
				} else if(nav.pl.fired && !nav.v.fs){
					var rh = $('.videolist .row').outerHeight();
					switch(evt.command){
						case "UP" :
							if(nav.v.row != 0){
								--nav.v.row;
								$(".videolist").css({top: $(".videolist").position().top += rh});
								pickVideo();
							}
							break;
						case "DOWN" : 
							if(nav.v.row+1 < $('.videolist .row').length){
								++nav.v.row;
								$(".videolist").css({top: $(".videolist").position().top -= rh});
								pickVideo();
							}
							break;
						case "RIGHT" : 
							if(nav.v.cv+1 < $($('.videolist .row')[nav.v.row]).find('.li').length){
								++nav.v.cv;
								pickVideo();
							}
							break;
						case "LEFT" : 
							if(nav.v.cv != 0){
								--nav.v.cv;
								pickVideo();
							}
							break;
						case "ENTER":
							player.loadVideoById($('.row .li.active').attr('data-videoid'), 0, "default");
							var requestFullScreen = player.a.requestFullscreen || player.a.msRequestFullscreen || player.a.mozRequestFullScreen || player.a.webkitRequestFullscreen;
							if (requestFullScreen) {
							    requestFullScreen.bind(player.a)();
							}
							nav.v.fs = true;
							$('#player').removeClass('hidden');
							$($('.playlist li')[nav.pl.active]).focus();
							break;
						case "BACK": 
							nextPageToken = '';
							nav.pl.fired = false;
							$('.videolist .li').removeClass('active');
							nav.v.row = 0;
							nav.v.cv = 0;
							break;
					}
				} else {
					console.log("EVT", evt);
					switch(evt.command){
						case "BACK": 
							$('#player').addClass('hidden');
							player.stopVideo();
							nav.v.fs = false;
							break;
					}
				}
			}
		});
	};
	$(document).ready(function(){
		init();
	});
});
Smaf.init('W60xBbHl0RB4ByZVqqelFcUOAgdbzCe1');